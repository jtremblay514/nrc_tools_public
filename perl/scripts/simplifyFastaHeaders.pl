#!/usr/bin/env perl

use strict;
use warnings;

use Getopt::Long;
use Iterator::FastaDb;
use Iterator::FastqDb;

my $usage=<<'ENDHERE';
NAME:
simplifyFastaHeaders.pl

PURPOSE:
Simplify fasta headers. For anvio.

INPUT:
--infile <string> : Sequence file
				
OUTPUT:
<STDOUT>          : Sequence file fasta.

NOTES:

BUGS/LIMITATIONS:
 
AUTHOR/SUPPORT:
National Research Council Canada - Genomics and Microbiomes
Julien Tremblay - julien.tremblay@nrc-cnrc.gc.ca

ENDHERE

## OPTIONS
my ($help, $infile);
my $verbose = 0;

GetOptions(
   'infile=s' => \$infile,
   'verbose'  => \$verbose,
   'help'     => \$help
);
if ($help) { print $usage; exit; }

## MAIN

open(IN, "<".$infile) or die "Can't open $infile\n";
while(<IN>){
   if($_ =~ m/^>/){
       chomp;
       my ($new_header) = $_ =~ m/^(\S+)/;
       print STDOUT $new_header."\n";
   
   }else{
       print STDOUT $_;
   }
}
exit;
