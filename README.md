## LICENSE AND COPYRIGHT
This software is Copyright (c) 2019,2020,2021,2022,2023 by the National Research Council of Canada (NRC) but is freely available for use without any warranty under the GNU GENERAL PUBLIC LICENSE (Version 3, 29 June 2007). Refer to wrapped tools for their credits and license information.
This license does not grant you the right to use any trademark, service mark, tradename, or logo of the Copyright Holder.
This license includes the non-exclusive, worldwide, free-of-charge patent license to make, have made, use, offer to sell, sell, import and otherwise transfer the Package with respect to any patent claims licensable by the Copyright Holder that are necessarily infringed by the Package. If you institute patent litigation (including a cross-claim or counterclaim) against any party alleging that the Package constitutes direct or contributory patent infringement, then this Artistic License to you shall terminate on the date that such litigation is filed.
Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES. THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## Summary
This repository contains scripts (Perl, Python and R) performing bioinformatic tasks. These tools are intended to work with its companion nrc_pipeline_public repository - https://bitbucket.org/jtremblay514/nrc_pipeline_public/src/master/

For the latest release (v1.3.2) pipeline, use https://bitbucket.org/jtremblay514/nrc_pipeline_public/src/1.3.2/
